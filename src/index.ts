type TypedArrayConstructor = typeof Uint8Array | typeof Uint8ClampedArray | typeof Int8Array
type TypedArrayObject = InstanceType<TypedArrayConstructor>
// we don't support more than 8 bit types, because of big-endian vs little-endian issues

const NodejsBuffer = (typeof Buffer == "function") ? Buffer : null // supported in Node (v5.10+), but not in any browser (as far as i know)
const AsciiDecoder = (typeof TextDecoder == "function") ? new TextDecoder('ascii') : null // supported in modern browsers, and in Node (v11.0+)

export const isSupported = /*IIFE*/ function checkSupport () {
    if (typeof Uint8Array != "function") return false
    if (NodejsBuffer) return true
    if (typeof btoa != "function") return false
    if (typeof atob != "function") return false
    if (!AsciiDecoder && !String.fromCharCode) return false
    if (!String.prototype.charCodeAt) return false
    return true
}()

export function typedArray_to_base64 (typedArray:TypedArrayObject) {
    // Node.js
    if (NodejsBuffer) return NodejsBuffer.from(typedArray).toString('base64')

    // Modern browsers
    if (AsciiDecoder) return btoa(AsciiDecoder.decode(typedArray))
 
    // Older browsers (maybe this isn't the fastest way, but it's compatible with oldest browsers that support typed arrays, besides Android 4 that has no fromCharCode)
    const arrayType = typedArray.constructor as TypedArrayConstructor
    if (arrayType.BYTES_PER_ELEMENT > 1) throw new Error ("Can't decode " + arrayType.name)
    if (arrayType == Int8Array) typedArray = new Uint8Array (typedArray)
    let ascii = ''
    for (let i=0; i<typedArray.length; i++) ascii += String.fromCharCode(typedArray[i])
    return btoa (ascii)
}

export function base64_to_typedArray (base64:string, arrayType:TypedArrayConstructor = Uint8Array) {
    // Node.js
    if (NodejsBuffer) return new arrayType (NodejsBuffer.from(base64, 'base64'))

    // Browsers
    if (arrayType.BYTES_PER_ELEMENT > 1) throw new Error ("Can't encode into " + arrayType.name)
    const ascii = atob (base64)
    const typedArray = new arrayType (ascii.length)
    for (let i=0; i<typedArray.length; i++) typedArray[i] = ascii.charCodeAt(i)
    // note: in case of signedInt, no need to manually convert numbers above MAX_INT to negative
    return typedArray

    // Unfortunately, `new TextEncoder('ascii')` doesn't encode to ASCII
    // As of Firefox 48 [...], Chrome 54 [...] and Opera 41, no other encoding types are available other than utf-8, in order to match the spec.
}


// Explanation: both String.fromCharCode & String.prototype.charCodeAt works with extended ASCII codes (AKA Windows-1252 or Latin-1)

// ToDo: post solution in https://stackoverflow.com/a/11562550/579024 https://stackoverflow.com/a/28291416/579024

// https://stackoverflow.com/questions/7296594/array-join-vs-string-concat (performance)