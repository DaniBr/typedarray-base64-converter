# TypedArray Base64 Converter
![license: MIT](https://badgen.net/npm/license/globalthis-polyfill)
![TypeScript](https://badgen.net/badge/icon/typescript?icon=typescript&label)
[![GitLab](https://badgen.net/badge/icon/gitlab?icon=gitlab&label)](https://gitlab.com/DaniBr/typedArray-base64-converter/)

## Introduction
[Base64](https://en.wikipedia.org/wiki/Base64) allows transfer and store numbers as text in shorter form than decimal digits.
See the demonstration below.  
This package provides functions to encode and decode binary data from [typed-array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Typed_arrays) to base64.  
Works on both NodeJS and Browsers.
 
## Limitations
For now, the package only supports 8 bit arrays, e.g. Uint8Array | Uint8ClampedArray | Int8Array. 
The reason for that is to avoid [big-endian<>little-endian](https://en.wikipedia.org/wiki/Endianness) problems. 
If there will be high demand to support more than 1 byte array, we will add it in a future version, in endianness safe way.
 
## Requirements
This should work on any Browser that [supports typed-arrays](https://caniuse.com/mdn-javascript_builtins_typedarray), excluding Android Browser v4-4.3, because it doesn't have [charCodeAt](https://caniuse.com/?search=charCodeAt) & [fromCharCode](https://caniuse.com/?search=fromCharCode).
 
## Installation
``` console
$ npm install --save typedarray-base64-converter
```
``` js
import { typedArray_to_base64, base64_to_typedArray } from "typedArray-base64-converter"
```
 
## Usage
### Example 1: unsigned integers
``` js
const myUintArray = new Uint8Array ([0,23,41,68,87,109,125,146,161,187,202,244])
// Uint8Array(12) [0,  23,  41,  68,  87, 109, 125, 146, 161, 187, 202, 244]

typedArray_to_base64 (myUintArray)
// 'ABcpRFdtfZKhu8r0'

base64_to_typedArray ('ABcpRFdtfZKhu8r0')
// Uint8Array(12) [0,  23,  41,  68,  87, 109, 125, 146, 161, 187, 202, 244]
```
### Example 2: signed integers
``` js
const myIntArray = new Int8Array ([125, -110, -95, -69, -54, -12])
// Int8Array(6) [ 125, -110, -95, -69, -54, -12 ]

typedArray_to_base64 (myIntArray)
// 'fZKhu8r0'

base64_to_typedArray ('fZKhu8r0', Int8Array)
// Int8Array(6) [ 125, -110, -95, -69, -54, -12 ]
```
 
## Demonstration
In comparison, of doing the same without base64 conversion:
 
``` js
JSON.stringify(typedArray_to_base64 (myUintArray))
// '"ABcpRFdtfZKhu8r0"'
 
JSON.stringify(myUintArray)
// '{"0":0,"1":23,"2":41,"3":68,"4":87,"5":109,"6":125,"7":146,"8":161,"9":187,"10":202,"11":244}'

JSON.stringify([...myUintArray])
// '[0,23,41,68,87,109,125,146,161,187,202,244]'
```

## Methods & Properties
### typedArray_to_base64 (typedArray)
Parameters:
 - **typedArray** - The original array that should be encoded.

Returns a base64 string.

### base64_to_typedArray (base64, arrayType?)
Parameters:
 - **base64** - The base64 string.
 - **arrayType** *(optional, default = Uint8Array)* - The constructor of the original array.

Returns the same typed array as the original.

### isSupported
A boolean constant
